from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from schemas.employee import Employee
from connections.sql import get_engine
from main import app
from settings import settings


client = TestClient(app)



engine = get_engine(settings)
Employee.metadata.drop_all(engine)
Employee.metadata.create_all(engine)

session = Session(engine)

session.begin()
USERS = [
    Employee(
        first_name="ftest01",
        last_name="ltest01",
        location=1,
        department=1,
        position=1,
        status=1,
        company=1,
    ),
    Employee(
        first_name="ftest02",
        last_name="ltest02",
        location=2,
        department=2,
        position=2,
        status=2,
        company=2,
    ),
    Employee(
        first_name="ftest03",
        last_name="ltest03",
        location=1,
        department=2,
        position=3,
        status=1,
        company=5,
    ),
    Employee(
        first_name="ftest04",
        last_name="ltest04",
        location=5,
        department=4,
        position=3,
        status=2,
        company=1,
    ),
    Employee(
        first_name="ftest05",
        last_name="ltest05",
        location=5,
        department=2,
        position=2,
        status=1,
        company=1,
    ),
]
session.add_all(USERS)
session.commit()
    


def test_get_all_employee():
    response = client.get("/api/search")
    response_js = response.json()
    assert response.status_code == 200
    assert response_js["count"] == len(USERS)

def test_api_data_leak():
    response = client.get("/api/search")
    assert response.status_code == 200

    response_js = response.json()
    for employee in response_js["data"]:
        assert not hasattr(employee, "id")

def test_query_location():
    value = 1
    response = client.get("/api/search", params={"location": value})
    assert response.status_code == 200

    response_js = response.json()
    for employee in response_js["data"]:
        assert employee["location"] == value


def test_query_company():
    value = 1

    response = client.get("/api/search", params={"company": value})
    assert response.status_code == 200

    response_js = response.json()
    for employee in response_js["data"]:
        assert employee["company"] == value


def test_query_position():
    value = 1

    response = client.get("/api/search", params={"position": value})
    assert response.status_code == 200

    response_js = response.json()
    for employee in response_js["data"]:
        assert employee["position"] == value


def test_query_department():
    value = 1

    response = client.get("/api/search", params={"department": value})
    assert response.status_code == 200
    
    response_js = response.json()
    for employee in response_js["data"]:
        assert employee["department"] == value


def test_query_status():
    value = [1, 2]

    response = client.get("/api/search", params={"status": value})
    assert response.status_code == 200

    response_js = response.json()
    for employee in response_js["data"]:
        assert employee["status"] in value


def test_pagination():
    response = client.get("/api/search", params={"page_offset": 1, "page_limit": 2})
    assert response.status_code == 200

    response_js = response.json()
    assert len(response_js["data"]) == 2
    assert response_js["data"][0]["first_name"] == "ftest03"


def test_dynamic_columns():
    value = ["position", "department"]
    response = client.get("/api/search", params={"keep": value})
    assert response.status_code == 200

    response_js = response.json()
    employee = response_js["data"][0]
    for key in employee.keys():
        assert key in value
    
    for key in value:
        assert employee.get(key) is not None
    

def test_rate_limit():
    settings.ENABLE_RATE_LIMIT = True
    for i in range(10):
        response = client.get("/api/search")
        if i < 5:
            assert response.status_code == 200
        else:
            assert response.status_code == 429