import redis

from settings import Settings


def get_redis_connection(settings: Settings):
    client = redis.Redis.from_url(f"redis://{settings.REDIS_USER}:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB}", decode_responses=True)
    client.ping()
    return client