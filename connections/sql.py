from typing import Dict
from sqlalchemy.engine import create_engine, Engine

from settings import Settings


def get_engine(
        settings: Settings,
        connect_args: Dict = {}
    ) -> Engine:
    driver = {
        "sqlite": "sqlite://",
        "mysql": "mysql+pymysql",
        "postgres": "postgresql+psycopg2",
    }.get(settings.DB_DRIVER)
    if settings.DB_DRIVER == "sqlite":
        db_uri = f"{driver}{settings.DB_HOST}"
    else:
        db_uri = f"{driver}://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
    engine = create_engine(db_uri, connect_args=connect_args)
    return engine