STATUS = {
    0: "active",
    1: "not started",
    2: "terminated",
}

POSITIONS = {
    0: "No position",
    1: "Assitant manager",
    2: "Director",
}

DEPARTMENTS = {
    0: "No department",
    1: "Sales",
    2: "Engineering",
    3: "HR",
    4: "Marketing",
    5: "Finance",
    6: "Operations",
    7: "IT",
    8: "Admin",
    9: "Legal",
    10: "Consulting",
}

LOCATIONS = {
    0: "No location",
    1: "Singapore",
    2: "Malaysia",
    3: "Indonesia",
    4: "Thailand",
    5: "Vietnam",
    6: "China",
    7: "Japan",
    8: "Korea",
    9: "Taiwan",
    10: "Philippines",
}

COMPANIES = {
    0: "Meta",
    1: "Amazon",
    2: "Google",
    3: "Facebook",
    4: "IBM",
    5: "Oracle",
    6: "Microsoft",
    7: "Tencent",
    8: "Alibaba",
    9: "Citi",
    10: "Yahoo",
}