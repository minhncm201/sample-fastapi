# Quick start
- Require a postgresql and redis to run
    
    - You can clone [my repo](https://gitlab.com/utilities2131547/docker) and execute bash file in `sh` dir to start those as containers

    - Redis/Postgres must settings username/password

- After setup postgres and redis create a `.env` with connection's info (host, port, usr, pwd, ...). `.env` file. Please see `settings.py` file for the required key name. See `data/sample.env` for reference
- Execute `./start.sh` to start the service. If you start as docker container, see #Dockerize section

# Dockerize
- Set host as `host.docker.internal` instead of `localhost` or `127.0.0.1` in `.env`
- Execute `docker build -t fastapi .`
- Execute `docker run --name fastapi -p 8000:8000 fastapi`

# Startup
- Service will create table and populate 10000 random user on first successful start. If for some reason that it fail to start, elete the table in database before start the serviec again.

# API
- All api is not require authentication or authorization to call.
- Go to `/docs` to see generated docs for the api

# Rate limiter
Implement as a middleware in `main.py`

Disable by settings `ENABLE_RATE_LIMIT` in `.env`

Default only allow 5 request per 30 seconds. update `MAX_LIMIT` and `EXPIRE_TIME` to change that

# Testing
- Disable rate limit
- Change `DB_DRIVER` to `sqlite` and `DB_HOST` to `/tmp/data.db`
- Execute `pytest -s`
