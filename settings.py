from pydantic_settings import BaseSettings
from dotenv import load_dotenv



class Settings(BaseSettings):
    DB_DRIVER: str = "sqlite"
    DB_HOST: str = "locahost"
    DB_PORT: int = 5432
    DB_NAME: str
    DB_USER: str
    DB_PASSWORD: str

    REDIS_HOST: str = "localhost"
    REDIS_PORT: int = 6379
    REDIS_DB: int = 0
    REDIS_USER: str
    REDIS_PASSWORD: str

    ENABLE_RATE_LIMIT: bool = True


dotenv_path = ".env"
load_dotenv(dotenv_path, override=True)
settings = Settings(_env_file=dotenv_path)