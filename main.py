import json
import random
import pendulum
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from sqlalchemy.engine import Engine
from redis import Redis

from connections.sql import get_engine
from connections.redis import get_redis_connection
from routers.api.employee import router as employee_router
from schemas.employee import Employee
from constants.enum import COMPANIES,  DEPARTMENTS, LOCATIONS, POSITIONS, STATUS
from settings import settings


app = FastAPI()

@app.on_event("startup")
async def event_startup():
    engine: Engine = get_engine(
        settings=settings,
    )
    session = Session(engine)
    connection = engine.connect()
    if not engine.dialect.has_table(connection, "employee"):
        session.begin()
        if not (settings.DB_DRIVER == "sqlite" and settings.DB_HOST.startswith("tmp")):
            Employee.metadata.create_all(engine)
            for _ in range(10_000):
                status = random.choice(range(len(STATUS)))
                position = random.choice(range(len(POSITIONS)))
                location = random.choice(range(len(LOCATIONS)))
                department = random.choice(range(len(DEPARTMENTS)))
                company = random.choice(range(len(COMPANIES)))
                first_name = random.choice(["John", "Jane", "Jack", "Jill", "Jim", "Jenny", "Jasmine", "Jocelyn", "Johanna", "Jenifer"])
                last_name = random.choice(["Doe", "Smith", "Johnson", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson"])
                employee = Employee(
                    first_name=first_name,
                    last_name=last_name,
                    location=location,
                    department=department,
                    position=position,
                    status=status,
                    company=company,
                )
                session.add(employee)
        session.commit()
    

@app.middleware("http")
async def rate_limiter(request: Request, call_next):
    if settings.ENABLE_RATE_LIMIT:
        KEY = f"LIMITER#{request.client.host}"
        MAX_LIMIT = 5
        EXPIRE_TIME = 30
        expire_at = pendulum.now().timestamp() + EXPIRE_TIME

        redis: Redis = get_redis_connection(settings)
        limit_info = redis.hgetall(KEY)

        if limit_info:
            limit_count = int(limit_info.get("count")) + 1
            if limit_count > MAX_LIMIT:
                return JSONResponse({"msg": f"reach limit for {EXPIRE_TIME} seconds"}, status_code=429)
        else:
            redis.hset(KEY, "expire", expire_at)
            redis.expire(KEY, EXPIRE_TIME)
            limit_count = 1

        redis.hset(KEY, "count", limit_count)

    response = await call_next(request)
    return response


app.include_router(employee_router)

json.dump(app.openapi(), open("data/openapi.json", "w+"))