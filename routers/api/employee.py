from typing import Any, List, Optional
from typing_extensions import Annotated
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy import select
from sqlalchemy import func as sql_func
from sqlalchemy.orm import Session

from payload.requests.api.employee import SearchEmployeePayload
from payload.response.api.employee import ResponseEmployeePayload
from connections.sql import get_engine
from settings import settings
from schemas.employee import Employee

router = APIRouter(prefix="/api")


@router.get("/search", tags=["employee-search"], description="API return users filter by status, potision, department, location, company.\n")
async def search_employee(
    query_params: Annotated[SearchEmployeePayload, Depends(SearchEmployeePayload)]
):
    try:
        filter_cond = []
        engine = get_engine(settings)
        session = Session(engine)
        if query_params.status is not None:
            filter_cond.append(Employee.status.in_(query_params.status))
        if query_params.department is not None:
            filter_cond.append(Employee.department == query_params.department)
        if query_params.position is not None:
            filter_cond.append(Employee.position == query_params.position)
        if query_params.location is not None:
            filter_cond.append(Employee.location == query_params.location)
        if query_params.company is not None:
            filter_cond.append(Employee.company == query_params.company)

        query_statement = select(Employee) \
            .where(*filter_cond) \
            .offset(query_params.page_offset * query_params.page_limit) \
            .limit(query_params.page_limit)
        count_statement = select(sql_func.count("*")).select_from(Employee).where(*filter_cond)
        
        query_count = session.scalar(count_statement)
        result = []
        for employee in session.scalars(query_statement):
            employee = ResponseEmployeePayload(**employee.__dict__).model_dump()
            if query_params.keep is not None:
                formatted_employee = {}
                for field in query_params.keep:
                    formatted_employee[field] = employee[field]
                employee = formatted_employee
            result.append(employee)
    except Exception as e:
        print(e)
        return JSONResponse({"msg": "server error"}, status_code=500)
    else:
        return JSONResponse({"data": result, "count": query_count})