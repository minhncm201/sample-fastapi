FROM python:3.11.2-slim-bullseye

WORKDIR /app
COPY . .

RUN pip install -r requirements.txt
EXPOSE 8000

ENTRYPOINT [ "./start.sh" ]