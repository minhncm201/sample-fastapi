import pendulum
from typing import Optional
from datetime import datetime
from sqlalchemy import ForeignKey
from sqlalchemy.types import String, Numeric, Boolean, DateTime, Integer
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    pass


class Employee(Base):
    __tablename__ = "employee"

    id: Mapped[int] = mapped_column(primary_key=True)
    first_name: Mapped[str] = mapped_column(String(64))
    last_name: Mapped[str] = mapped_column(String(64))
    contact_info: Mapped[str] = mapped_column(String(128), nullable=True)

    status: Mapped[int] = mapped_column(Integer, index=True)
    position: Mapped[int] = mapped_column(Integer, index=True)
    location: Mapped[int] = mapped_column(Integer, index=True)
    department: Mapped[int] = mapped_column(Integer, index=True)
    company: Mapped[int] = mapped_column(Integer, index=True)

    created_at: Mapped[Optional[datetime]] = mapped_column(DateTime, default=pendulum.now())
    updated_at: Mapped[Optional[datetime]] = mapped_column(DateTime, default=pendulum.now(), onupdate=pendulum.now())

    def __repr__(self) -> str:
        return f"{self.first_name} - {self.last_name}"
