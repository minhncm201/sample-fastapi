from typing import Optional, List
from pydantic import BaseModel, Field
from fastapi import Query
from constants.enum import STATUS


class SearchEmployeePayload(BaseModel):
    location: Optional[int] = Field(Query(None, description="Foreign key of location table. Default to None, which will select all. Ex: locations=1"))
    company: Optional[int] = Field(Query(None, description="Foreign key of company table. Default to None, which will select all. Ex: companies=1"))
    department: Optional[int] = Field(Query(None, description="Foreign key of department table. Default to None, which will select all. Ex: departments=1"))
    position: Optional[int] = Field(Query(None, description="Foreign key of position table. Default to None, which will select all. Ex: positions=1"))
    status: Optional[List[int]] = Field(Query(None, description=f"User's accoutn status. Default to None, which will select all. Only accept {STATUS.keys()}. Ex: status=1&status=2"))
    keep: Optional[List[str]] = Field(Query(None, description=f"List of key will be return in response if set. Default to None, which will return all allowed keys. Ex: keep=locations&keep=status"))
    page_offset: Optional[int] = Field(Query(0, description=f"Page index use to skip data. Default to 0. Ex: 1 will return from the tenth data (1 * page_limit)"))
    page_limit: Optional[int] = Field(Query(10, description=f"Max users will be return in response payload. Default to 10"))