from pydantic import BaseModel


class ResponseEmployeePayload(BaseModel):
    first_name: str
    last_name: str
    status: int
    department: int
    location: int
    company: int
    position: int
